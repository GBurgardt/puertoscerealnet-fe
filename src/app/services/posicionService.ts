
// Angular
import { Injectable } from "@angular/core";
import { AuthService } from "src/app/services/authService";
import { LocalStorageService } from "./localStorageService";
import { map, take } from "rxjs/operators";
import { UtilsService } from "src/app/services/utilsService";
import { constants } from "src/app/environment/constants";
import { AccionDialog } from 'src/app/components/others/accionDialog/accionDialog';
import { MatDialog } from '@angular/material';
import { CartaPorte } from 'src/app/models/cartaPorte';
import { Observable } from 'rxjs';
import { FilesService } from 'src/app/services/filesService';
import { NotificacionesService } from "src/app/services/notificacionesService";

@Injectable()
export class PosicionService {

    constructor(
        private authService: AuthService,
        private utilsService: UtilsService,
        public dialog: MatDialog,
        private localStorageService: LocalStorageService,
        public filesService: FilesService,
        private notificacionesService: NotificacionesService
    ){ }

    /**
     * Obtiene la posicion de ciertos entregadores

     */
    getPosicion = () => {
        try {
            const posicionOrdenada = this.orderPosicion(this.authService.getPosicion());

            return posicionOrdenada;
        }
        catch(err) {
            console.log(err);
        }
    }

    /**
     * Ordena la posicion de acuerdo a un criterio especial de la web vieja
     */
    private orderPosicion = (posicionObserv: Observable<CartaPorte[]>) => posicionObserv.pipe(
        map(
            posicion => {
                const newPos = posicion
                    .sort(
                        (cp1,cp2) => cp1.puertoDestino.ptoCodinterno - cp2.puertoDestino.ptoCodinterno
                    )
                return newPos;
            }
        )
    )

    onClickCarta = (carta: CartaPorte, cartasSeleccionadas: CartaPorte[]) => {
        // const previoSeleccionada = cartasSeleccionadas.some(
        //     c => c.PorteNro === carta.PorteNro && c.analisis.anl_rubro === carta.analisis.anl_rubro
        // );

        // return previoSeleccionada ? cartasSeleccionadas.filter(
        //     c => !(c.PorteNro === carta.PorteNro && c.analisis.anl_rubro === carta.analisis.anl_rubro)
        // ) : cartasSeleccionadas.concat(carta);

        const previoSeleccionada = cartasSeleccionadas.some(
            c =>    c.PorteNro === carta.PorteNro &&
                    c.porteVagon === carta.porteVagon &&
                    c.porteCodPuerto === carta.porteCodPuerto &&
                    c.entregador.cuit === carta.entregador.cuit
        );

        return previoSeleccionada ? cartasSeleccionadas.filter(
            c => !( c.PorteNro === carta.PorteNro &&
                    c.porteVagon === carta.porteVagon &&
                    c.porteCodPuerto === carta.porteCodPuerto &&
                    c.entregador.cuit === carta.entregador.cuit)
        ) : cartasSeleccionadas.concat(carta);

    }


    /**
     * Abre el dialog de accion
     */
    openAccionDialog = (cartasSeleccionadas: CartaPorte[], actualizarData) => {
        let dialogRef = this.dialog.open(AccionDialog, {
            data: {
                cartasSeleccionadas: cartasSeleccionadas
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            // Actualizo
            actualizarData(
                result,
                Object.assign([], cartasSeleccionadas)
            );
        });
    }

    /**
     * Determina si se le puede aplicar una acción a una carta dada
     */
    checkIfAccionable = (carta: CartaPorte) => (
        this.localStorageService.getUsuarioActivo() &&
        (!carta.entreCp || carta.entreCp.length === 0) &&
        (
            (carta.porteEstado === 5 && carta.estado.estado === 0) ||
            (carta.porteEstado === 3 && carta.estado.estado === 1)
        ) &&
        (carta.porteTipo !== 3)
    )


    /**
     *
     */
    processInfiniteScroll = (posicionTotal: CartaPorte[]) => (dataSource) => (estadoTablaPosicion) => {
        // Solo procezar el infinite scroll si no hay filtros activos ni descarga activa
        if (
            estadoTablaPosicion === constants.estadosTablaPosi.INFINITE_SCROLL
        ) {
            const sidenavBody = document.getElementsByClassName('sidenav-body mat-drawer-content mat-sidenav-content');

            if (sidenavBody && sidenavBody[0]){

                const posicionFinal = sidenavBody[0].scrollHeight - sidenavBody[0].clientHeight;
                const scrollTop = sidenavBody[0].scrollTop;

                if (posicionFinal - scrollTop <= 0) {
                    dataSource.getCartasSubject()
                        .pipe(
                            take(1)
                        )
                        .subscribe(tandaActual => {

                            const nuevoIndex = (tandaActual.length + constants.infiniteScrollSize) <= posicionTotal.length ? (tandaActual.length + constants.infiniteScrollSize) : posicionTotal.length;

                            // Por ahora saco desde el principio hasta el nuevo tamaño. Luego, puedo sacar desde tamaño anterior hasta el nuevo...
                            const nuevaTanda = posicionTotal.slice(0, nuevoIndex);

                            // Actualizo tanda actual
                            dataSource.setPosicion(nuevaTanda);

                        });
                }
            };
        }

    }

    /**
     * Columna pat/vag de grilla posicion
     */
    getPatVag = (row: CartaPorte) => !row.porteVagon || row.porteVagon === 0 ?
        row.portePatenteCamion : row.porteVagon


    /**
     * Setea/des-setea una carta en tramite
     */
    setEnTramite = (row: CartaPorte, newState: boolean) => this.authService.setEnTramite(row, newState);

}

