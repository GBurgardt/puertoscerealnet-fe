// Angular
import { Injectable } from "@angular/core";
import { AuthService } from "src/app/services/authService";
import { LocalStorageService } from "./localStorageService";

@Injectable()
export class LoginService {

    constructor(
        private authService: AuthService,
        private localStorageService: LocalStorageService
    ){ }

    /**
     * Loguea, guarda el token en localStorage y retorna avisando si se hizo bien o mal
     */
    doLogin = async(dataUser: {nombre: string, clave: string}) => {
        let estadoLogin = false;
        try {
            // Hago el login y obtengo el acceso activo
            const accesoActivo = await this.authService.login(dataUser.nombre)(dataUser.clave);
            // Guardo el acceso activo
            this.localStorageService.setObject('accesoActivo')(accesoActivo);
            // this.localStorageService.guardarDatosAcceso(accesoActivo);

            estadoLogin = true;
        }
        catch(err) {
            throw(err);
        }

        return estadoLogin;
    }


    getBackgroundUrl = () => {
        //return 'background: url(assets/img/fondoLogin.jpg) no-repeat center center fixed; background-size: cover;';
        return "url('assets/img/fondoLogin.jpg') no-repeat center center fixed";
    }
}
