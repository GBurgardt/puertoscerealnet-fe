import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LoginComponent } from 'src/app/components/login/login.component';
import { HomeComponent } from 'src/app/components/home/home.component';
import { PosicionComponent } from './components/posicion/posicion.component';
import { HistoriaComponent } from 'src/app/components/historia/historia.component';
import { BuscarCartaComponent } from 'src/app/components/buscarCarta/buscarCarta.component';
import { DetalleCartaComponent } from 'src/app/components/detalleCarta/detalleCarta.component';
import { HistorialAccionesComponent } from 'src/app/components/historialAcciones/historialAcciones.component';
import { BuscarCartaPatenteComponent } from './components/buscarCartaPatente/buscarCartaPatente.component';
import { CamionesDescargaComponent } from './components/camionesDescarga/camionesDescarga.component';
import { CondicionesReciboComponent } from './components/condicionesRecibo/condicionesRecibo.component';
import { HistorialNoticiasComponent } from './components/historialNoticias/historialNoticias.component';


export const routes: Routes = [
    {
        path: 'login',
        loadChildren: 'src/app/components/login/login.module#LoginModule'
    },
    {

        path: 'home',
        loadChildren: 'src/app/components/home/home.module#HomeModule'
    },

    // {
    //     path: 'home',
    //     component: HomeComponent,
    //     children: [
    //         { path: 'posicion', component: PosicionComponent },
    //         { path: 'historia', component: HistoriaComponent },
    //         { path: 'buscar-carta', component: BuscarCartaComponent },
    //         { path: 'buscar-patente', component: BuscarCartaPatenteComponent },
    //         { path: 'detalle-carta/:nroCarta', component: DetalleCartaComponent },
    //         { path: 'historial-acciones', component: HistorialAccionesComponent },
    //         { path: 'historial-noticias', component: HistorialNoticiasComponent },
    //         { path: 'camiones-descarga', component: CamionesDescargaComponent },
    //         { path: 'condiciones-recibo', component: CondicionesReciboComponent },
    //         { path: '', redirectTo: 'posicion', pathMatch: 'full'},
    //     ]
    // },
    { path: '', redirectTo: '/login', pathMatch: 'full'}
    // { path: '**', component: LoginComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });
