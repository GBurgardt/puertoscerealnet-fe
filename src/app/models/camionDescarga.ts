export class CamionDescarga {
    maiz: number;
    soja: number;
    trigo: number;
    girasol: number;
    sorgo: number;
    otros: number;
    puerto: string;
    observacion: string

    constructor(camionDescarga?: {
        maiz: number;
        soja: number;
        trigo: number;
        girasol: number;
        sorgo: number;
        otros: number;
        puerto: string;
        observacion: string
    }) {
        if (camionDescarga) {
            this.maiz = camionDescarga.maiz;
            this.soja = camionDescarga.soja;
            this.trigo = camionDescarga.trigo;
            this.girasol = camionDescarga.girasol;
            this.sorgo = camionDescarga.sorgo;
            this.otros = camionDescarga.otros;
            this.puerto = camionDescarga.puerto;
            this.observacion = camionDescarga.observacion
        } else {
            this.maiz = null;
            this.soja = null;
            this.trigo = null;
            this.girasol = null;
            this.sorgo = null;
            this.otros = null;
            this.puerto = null;
            this.observacion = null
        }
    }
}
