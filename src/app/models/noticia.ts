import { Puerto } from "./puerto";

export class Noticia {
    id: number;
    titulo: string;
    descripcion: string;
    fechaHora: string;
    puerto: Puerto
    vista: boolean;

    constructor(noticia?: {
        id: number;
        titulo: string;
        descripcion: string;
        fechaHora: string;
        puerto: any
    }) {
        if (noticia) {
            this.id = noticia.id;
            this.titulo = noticia.titulo;
            this.descripcion = noticia.descripcion;
            this.fechaHora = noticia.fechaHora;
            this.puerto = new Puerto(noticia.puerto);
            this.vista = false;
        } else {
            this.id = null;
            this.titulo = null;
            this.descripcion = null;
            this.fechaHora = null;
            this.puerto = new Puerto();
            this.vista = null;
        }
    }

}
