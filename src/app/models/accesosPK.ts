export class AccesosPK {
    nombreUsuario: string;
    fecha: string

    constructor(accesosPK?: {
        nombreUsuario: string;
        fecha: string
    }) {
        if (accesosPK) {
            this.nombreUsuario = accesosPK.nombreUsuario;
            this.fecha = accesosPK.fecha
        } else {
            this.nombreUsuario = null;
            this.fecha = null
        }
    }

}
