export class Calidad {
    cldCodigo: number;
    cldDescripcion: string;
    cldNomenclatura: string

    constructor(calidad?: {
        cldCodigo: number;
        cldDescripcion: string;
        cldNomenclatura: string
    }) {
        if (calidad) {
            this.cldCodigo = calidad.cldCodigo;
            this.cldDescripcion = calidad.cldDescripcion;
            this.cldNomenclatura = calidad.cldNomenclatura
        } else {
            this.cldCodigo = null;
            this.cldDescripcion = null;
            this.cldNomenclatura = null
        }
    }

}
