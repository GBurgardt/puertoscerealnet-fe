import { AccesosPK } from "src/app/models/accesosPK";

export class Acceso {
    accesosPK: AccesosPK;
    logueo: string;
    resultado: string;
    ip: string;
    token: string;
    fechaHasta: string

    constructor(acceso?: {
        accesosPK: any;
        logueo: string;
        resultado: string;
        ip: string;
        token: string;
        fechaHasta: string
    }) {
        if (acceso) {
            this.accesosPK = new AccesosPK(acceso.accesosPK);
            this.logueo = acceso.logueo;
            this.resultado = acceso.resultado;
            this.ip = acceso.ip;
            this.token = acceso.token;
            this.fechaHasta = acceso.fechaHasta
        } else {
            this.accesosPK = new AccesosPK(null);
            this.logueo = null;
            this.resultado = null;
            this.ip = null;
            this.token = null;
            this.fechaHasta = null
        }
    }

}
