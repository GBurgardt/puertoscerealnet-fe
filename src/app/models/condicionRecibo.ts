export class CondicionRecibo {
    puerto: string;
    maiz: string;
    soja: string;
    trigo: string;
    girasol: string;
    sorgo: string;
    otros: string

    constructor(condicionRecibo?: {
        puerto: string;
        maiz: string;
        soja: string;
        trigo: string;
        girasol: string;
        sorgo: string;
        otros: string
    }) {
        if (condicionRecibo) {
            this.puerto = condicionRecibo.puerto;
            this.maiz = condicionRecibo.maiz;
            this.soja = condicionRecibo.soja;
            this.trigo = condicionRecibo.trigo;
            this.girasol = condicionRecibo.girasol;
            this.sorgo = condicionRecibo.sorgo;
            this.otros = condicionRecibo.otros
        } else {
            this.puerto = null;
            this.maiz = null;
            this.soja = null;
            this.trigo = null;
            this.girasol = null;
            this.sorgo = null;
            this.otros = null
        }
    }
}
