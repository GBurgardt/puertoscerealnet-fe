import { Puerto } from "./puerto";
import { Especie } from "src/app/models/especie";

export class Accion {
    nroCarta: number;
    accion: string;
    cuitEntregador: number;
    usuario: string;
    fecha: string;
    estado: string;
    puertoDesvio: Puerto;
    puerto: Puerto;
    comentario: string;
    turno: number;
    especie: Especie;

    constructor(accion?: {
        nroCarta: number;
        accion: string;
        cuitEntregador: number;
        usuario: string;
        fecha: string;
        estado: string;
        puertoDesvio: Puerto;
        puerto: Puerto;
        comentario: string;
        turno: number;
        especie: any;
    }) {
        if (accion) {
            this.nroCarta = accion.nroCarta;
            this.accion = accion.accion;
            this.cuitEntregador = accion.cuitEntregador;
            this.usuario = accion.usuario;
            this.fecha = accion.fecha;
            this.estado = accion.estado;
            this.puertoDesvio = new Puerto(accion.puertoDesvio);
            this.puerto = new Puerto(accion.puerto);
            this.comentario = accion.comentario;
            this.turno = accion.turno;
            this.especie = new Especie(accion.especie);
        } else {
            this.nroCarta = null;
            this.accion = null;
            this.cuitEntregador = null;
            this.usuario = null;
            this.fecha = null;
            this.estado = null;
            this.puertoDesvio = new Puerto();
            this.puerto = new Puerto();
            this.comentario = null;
            this.turno = null;
            this.especie = null;
        }
    }

}
