export class Puerto {
    ptoCodinterno: number;
    ptoCuit: number;
    ptoPostalonnca: number;
    ptoRazon: string;
    ptoDomicilio: string;
    ptoTurno: string;
    ptoLocalOncca: number;
    ptoControlTurno: string;
    ptoPlanta: number;
    ptoSeleccion: number;
    ptoTipo: number

    constructor(puerto?: {
        ptoCodinterno: number;
        ptoCuit: number;
        ptoPostalonnca: number;
        ptoRazon: string;
        ptoDomicilio: string;
        ptoTurno: string;
        ptoLocalOncca: number;
        ptoControlTurno: string;
        ptoPlanta: number;
        ptoSeleccion: number;
        ptoTipo: number
    }) {
        if (puerto) {
            this.ptoCodinterno = puerto.ptoCodinterno;
            this.ptoCuit = puerto.ptoCuit;
            this.ptoPostalonnca = puerto.ptoPostalonnca;
            this.ptoRazon = puerto.ptoRazon;
            this.ptoDomicilio = puerto.ptoDomicilio;
            this.ptoTurno = puerto.ptoTurno;
            this.ptoLocalOncca = puerto.ptoLocalOncca;
            this.ptoControlTurno = puerto.ptoControlTurno;
            this.ptoPlanta = puerto.ptoPlanta;
            this.ptoSeleccion = puerto.ptoSeleccion;
            this.ptoTipo = puerto.ptoTipo
        } else {
            this.ptoCodinterno = null;
            this.ptoCuit = null;
            this.ptoPostalonnca = null;
            this.ptoRazon = null;
            this.ptoDomicilio = null;
            this.ptoTurno = null;
            this.ptoLocalOncca = null;
            this.ptoControlTurno = null;
            this.ptoPlanta = null;
            this.ptoSeleccion = null;
            this.ptoTipo = null
        }
    }

}
