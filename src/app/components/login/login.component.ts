import { Component } from '@angular/core';
import { LoginService } from '../../services/loginService';
import { Router } from '@angular/router';
import { LocalStorageService } from '../../services/localStorageService';
import { UtilsService } from 'src/app/services/utilsService';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    // Data del user a loguearse
    dataUser: {nombre: string, clave: string} = {nombre: '', clave: ''};

    spinner = false;

    constructor(
        public loginService: LoginService,
        private router: Router,
        private localStorageService: LocalStorageService,
        private utilsService: UtilsService
    ) { }

    /**
     * Hace el login
     */
    doLogin = async() => {
        try {
            // Activo spinner
            this.spinner = true;
            // Hago login y obtengo si se hizo bien o mal
            const estadoLogin = await this.loginService.doLogin(this.dataUser);
            // Desactivo spinner
            this.spinner = false;
            // Si es true (se hizo bien), redirecciono a home
            (estadoLogin) ? this.completeLogin() : null
        }
        catch(err) {
            // Desactivo spinner
            this.spinner = false;
            this.utilsService.openDefaultDialog('Error')(err.error.descripcion);
            console.log(err.error)
        }
    }

    /**
     * Metodo de ayuda, completa el login
     */
    private completeLogin = () => {
        // Activo el menu
        this.localStorageService.setObject('leftMenu')(true);
        // Redirecciono a home
        this.router.navigate(['/home/posicion']);
    }

}
