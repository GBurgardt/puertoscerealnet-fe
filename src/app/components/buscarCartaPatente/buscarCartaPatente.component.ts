import { Component, OnInit } from '@angular/core';
import { CartaPorteDetalle } from '../../models/cartaPorteDetalle';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/authService';
import { finalize } from '../../../../node_modules/rxjs/operators';

@Component({
    selector: 'buscar-carta-patente',
    templateUrl: './buscarCartaPatente.component.html',
    styleUrls: ['./buscarCartaPatente.component.scss']
})
export class BuscarCartaPatenteComponent {
    patenteBuscada: string;

    cartasEncontradas: Observable<any[]>;

    estado: string = 'porBuscar';

    constructor(
        private router: Router,
        private authService: AuthService
    ) { }

    onClickBuscar = () => {

        this.estado = 'cargando';

        this.cartasEncontradas = this.authService.getHistoriaPorNroOrPatente(this.patenteBuscada)
            .pipe(
                finalize(() => this.estado = 'listo')
            );
    }
}
