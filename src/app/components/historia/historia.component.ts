import { take } from 'rxjs/operators';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { HistoriaDataSource } from './historia-datasource';
import { AuthService } from '../../services/authService';
import { FiltrosModel } from 'src/app/models/filtrosModel';
import { ListaDespModel } from 'src/app/models/listaDespModel';
import { FilesService } from '../../services/filesService';
import { Estado } from '../../models/estado';
import { Puerto } from 'src/app/models/puerto';
import { TipoAccion } from 'src/app/models/tipoAccion';
import { CartaPorte } from 'src/app/models/cartaPorte';
import { HistoriaService } from 'src/app/services/historiaService';
import { UtilsService } from '../../services/utilsService';
import { CartaPorteService } from 'src/app/services/cartaPorteService';
import { CustomDateAdapter, APP_DATE_FORMATS } from 'src/app/components/others/customDateAdapter/customDateAdapter';
import { constants } from 'src/app/environment/constants';


@Component({
    selector: 'historia',
    templateUrl: './historia.component.html',
    styleUrls: ['./historia.component.scss'],
    providers: [
        {
            provide: DateAdapter, useClass: CustomDateAdapter
        },
        {
            provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
        }
    ]
})
export class HistoriaComponent implements OnInit {
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    dataSource: HistoriaDataSource;

    // Columnas
    displayedColumns  = [
        'nroCP',
        'situacion',
        'turno',
        'titular',
        'intermediario',
        'remitenteComercial',
        'corredor',
        'producto',
        'destino',
        'analisis',
        'obsEstado',
        'procedencia',
        'kgsProcedencia',
        'kgsNeto',
        'entregador',
        'entreCp',
        'autPuerto'
    ]

    historiaTotal: CartaPorte[] = [];

    // Objeto contenedor de las listas desplegables
    listasDesp: ListaDespModel = new ListaDespModel();

    // Los filtros activos actualmente (Siempre se filtra sobre la poscionTotal)
    filtrosActivos: FiltrosModel = new FiltrosModel();

    // Total de cartas activas (este se actualzia cuando se filtra)
    totalActivo: number = 0;

    // Cartas seleccionadas para hacer alguna accion
    cartasSeleccionadas: CartaPorte[] = [];

    // Boolean spinner
    descargaActiva: boolean = false;

    // Los filtros de las fechas para filtrar la historia
    filtroFecha: {
        fechaDesde: Date,
        fechaHasta: Date
    } = { fechaDesde: null, fechaHasta: null };

    // Boolean spinner
    estadoTablaHistoria: number = constants.estadosTablaPosi.INFINITE_SCROLL;

    constructor(
        private authService: AuthService,
        public historiaService: HistoriaService,
        public filesService: FilesService,
        public cartaPorteService: CartaPorteService,
        private utilsService: UtilsService
    ) { }

    ngOnInit() {
        // Inicializo algunos campos importantes de esta page
        this.initTable();
    };

    initTable = () => {
        // Inicializo la fecha seleccionada
        this.filtroFecha = this.historiaService.getFechasIniciales();
        // Inicializo el dataSource
        this.dataSource = new HistoriaDataSource(
            this.paginator,
            this.sort,
            this.historiaService
        );

        this.actualizarDataTabla();
    }

    /**
     * Hace una nueva consulta, y carga los arrays que se usan en esta clase, junto con el total
     */
    actualizarDataTabla = () => {
        // Checkeo las fechas
        if (this.historiaService.checkValidesFechas(this.filtroFecha)) {

            // Hago la primer consulta al service
            this.dataSource.loadHistoriaByFecha(this.filtroFecha);

            // Agarro solo el valor INICIAIL. Cuando haya cambios en el observable acá abajo no se van a estar escuchando
            // Hago take(2) poruqe primero dispara un array vacio, en la segunda recien dispara el array completo
            this.dataSource.getCartasSubject().pipe(
                take(2)
            ).subscribe(
                async(historia) => {
                    this.historiaTotal = historia;
                    // Inicializo las listas desplegables
                    this.listasDesp = await this.cartaPorteService.getArraysIniciales(historia);
                    // Obtengo la cantidad inicial del total
                    this.totalActivo = historia.length;

                }
            )
        } else {
            this.utilsService.openDefaultDialog('Hay un problema')('El intervalo de fechas es muy largo, el máximo de días es 3')
        }

    }

    /**
     * Filtrar las cartas
     */
    filtrar = (filtro, tipoFiltro) => {
        // Seteo los filtros activos
        this.filtrosActivos = this.cartaPorteService.getFiltrosActivos(this.filtrosActivos)(tipoFiltro)(filtro);

        // Filtro
        const histoFiltrada = this.cartaPorteService.filtrar(this.historiaTotal, this.filtrosActivos);

        // Actualizo el estado de la tabla. Tambien checkeo si se sacaron todos los filtros (la tabla vuelve a tener la posicion total)
        if (histoFiltrada.length < this.historiaTotal.length) {
            // Seteo nueva posicion filtrada
            this.dataSource.setHistoria(histoFiltrada);
            // Actualizo estado
            this.estadoTablaHistoria = constants.estadosTablaPosi.FILTRADA;
        } else {
            // Seteo posicion total (agarro solo 30 primeras cartas)
            // this.dataSource.setHistoria(histoFiltrada.slice(0, 30));
            // Por ahora no activo infnite scroll aca en historia, asi que seteo toda la posicion directamente
            this.dataSource.setHistoria(histoFiltrada);
            // Actualizo estado
            this.estadoTablaHistoria = constants.estadosTablaPosi.INFINITE_SCROLL;
        }


        // Las seteo en el subject (hace un next en el observable)
        // this.dataSource.setHistoria(posiFiltrada);

        // Actualizo el total activo
        this.totalActivo = histoFiltrada.length;
    }


    /**
     * Genera el excel
     */
    generarExcel = () => {
        // Guardo el estado actual
        const estadoAnterior = this.estadoTablaHistoria;
        // Ahora pongo el estado en Descargando
        this.estadoTablaHistoria = constants.estadosTablaPosi.DESCARGANDO;
        // Genero el excel
        this.cartaPorteService.generarExcel(this.dataSource)(estadoAnterior)('historia')(this.filtroFecha.fechaDesde)(this.filtroFecha.fechaHasta)
            .subscribe(
                nuevoEstado => this.estadoTablaHistoria = nuevoEstado
            );
    }

    /**
     * Actualial a data despues de ejercer una accion
     */
    actualizarDataDspsDeAccion = (resultado: {
        puertoNuevo: Puerto,
        comentario: string,
        tipoAccion: TipoAccion
    }) => {
        const dataActualizada = this.historiaTotal.map(carta => {
            const newCarta = this.cartasSeleccionadas.find(
                cartaSelec =>   carta.PorteNro === cartaSelec.PorteNro &&
                                carta.portePrefijo === cartaSelec.portePrefijo &&
                                carta.entregador.cuit === cartaSelec.entregador.cuit &&
                                carta.porteVagon === cartaSelec.porteVagon &&
                                carta.porteCodPuerto === cartaSelec.porteCodPuerto
            );
            if (newCarta) {

                const nuevoEstado =     resultado.tipoAccion.tipo === 1 ? 10 :
                                        resultado.tipoAccion.tipo === 2 ? 11 :
                                        0;

                const nuevoEstadoDescrip =      resultado.tipoAccion.tipo === 1 ? 'Entrega Autorizado' :
                                                resultado.tipoAccion.tipo === 2 ? 'Entrega Desviado' :
                                                'Posicion';

                //const estadoReal = '';

                newCarta.estado = new Estado({estado: nuevoEstado, descripcion: nuevoEstadoDescrip});
                newCarta.estadoPosiReal = nuevoEstadoDescrip;
                //
                return newCarta;
            } else {
                return carta;
            }
        });

        this.dataSource.setHistoria(dataActualizada);
    }


}

