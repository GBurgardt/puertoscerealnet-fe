import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map, catchError, finalize } from 'rxjs/operators';
import { Observable, of as observableOf, merge, BehaviorSubject, of } from 'rxjs';


import { CartaPorte } from 'src/app/models/cartaPorte';
import { HistoriaService } from '../../services/historiaService';


export class HistoriaDataSource extends DataSource<CartaPorte> {
    private historiaItemSubject = new BehaviorSubject<CartaPorte[]>([]);
    private loadingSubject = new BehaviorSubject<any>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(
        private paginator: MatPaginator,
        private sort: MatSort,
        private historiaService: HistoriaService
    ) {
        super();
    }

    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    connect(collectionViewer: CollectionViewer): Observable<CartaPorte[]> {
        return this.historiaItemSubject.asObservable();
    }

    /**
     *  Called when the table is being destroyed. Use this function, to clean up
     * any open connections or free any held resources that were set up during connect.
     */
    disconnect() {
        // this.historiaItemSubject.complete();
        // this.loadingSubject.complete();
    }


    loadHistoriaByFecha(filtroFecha) {

        this.loadingSubject.next(true);

        this.historiaService.getHistoria(filtroFecha).pipe(
            catchError(() => of([])),
            finalize(() => this.loadingSubject.next(false))
        )
        .subscribe(historia => this.historiaItemSubject.next(historia));
    }

    /**
     * Getter para obtener el subject con la historia
     */
    getCartasSubject = () => this.historiaItemSubject;

    /**
     * Settear nuevo valor a la historia
     */
    setHistoria = (historia: CartaPorte[]) => this.historiaItemSubject.next(historia);


    getSpinnerSubject = () => this.loadingSubject;
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
