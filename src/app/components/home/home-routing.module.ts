import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { PosicionComponent } from '../posicion/posicion.component';
import { HistoriaComponent } from '../historia/historia.component';
import { BuscarCartaComponent } from '../buscarCarta/buscarCarta.component';
import { BuscarCartaPatenteComponent } from '../buscarCartaPatente/buscarCartaPatente.component';
import { DetalleCartaComponent } from '../detalleCarta/detalleCarta.component';
import { HistorialAccionesComponent } from '../historialAcciones/historialAcciones.component';
import { HistorialNoticiasComponent } from '../historialNoticias/historialNoticias.component';
import { CamionesDescargaComponent } from '../camionesDescarga/camionesDescarga.component';
import { CondicionesReciboComponent } from '../condicionesRecibo/condicionesRecibo.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: 'posicion',
                loadChildren: 'src/app/components/posicion/posicion.module#PosicionModule'
                // component: PosicionComponent
            },
            { path: 'historia', component: HistoriaComponent },
            { path: 'buscar-carta', component: BuscarCartaComponent },
            { path: 'buscar-patente', component: BuscarCartaPatenteComponent },
            { path: 'detalle-carta/:nroCarta', component: DetalleCartaComponent },
            { path: 'historial-acciones', component: HistorialAccionesComponent },
            { path: 'historial-noticias', component: HistorialNoticiasComponent },
            { path: 'camiones-descarga', component: CamionesDescargaComponent },
            { path: 'condiciones-recibo', component: CondicionesReciboComponent },
            { path: '', redirectTo: 'posicion', pathMatch: 'full'},
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
