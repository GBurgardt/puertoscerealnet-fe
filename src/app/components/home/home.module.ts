import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module';
import { MenuComponent } from '../menu/menu.component';
import { PosicionComponent } from '../posicion/posicion.component';
import { HistoriaComponent } from '../historia/historia.component';
import { BuscarCartaComponent } from '../buscarCarta/buscarCarta.component';
import { DetalleCartaComponent } from '../detalleCarta/detalleCarta.component';
import { HistorialAccionesComponent } from '../historialAcciones/historialAcciones.component';
import { HistorialNoticiasComponent } from '../historialNoticias/historialNoticias.component';
import { BuscarCartaPatenteComponent } from '../buscarCartaPatente/buscarCartaPatente.component';
import { CamionesDescargaComponent } from '../camionesDescarga/camionesDescarga.component';
import { CondicionesReciboComponent } from '../condicionesRecibo/condicionesRecibo.component';
import { MatBadgeModule, MatCheckboxModule, MatTableModule, MatPaginatorModule, MatSortModule, MatButtonModule, MatSidenavModule, MatToolbarModule, MatCardModule, MatFormFieldModule, MatInputModule, MatProgressSpinnerModule, MatListModule, MatSelectModule, MatMenuModule, MatRadioModule, MatDatepickerModule, MatNativeDateModule, MatTooltipModule } from '@angular/material';
import { ClipboardModule } from 'ngx-clipboard';
import { PosicionService } from 'src/app/services/posicionService';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HomeRoutingModule,
        ReactiveFormsModule,
        MatBadgeModule,
        MatCheckboxModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatButtonModule,
        MatSidenavModule,
        MatToolbarModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatListModule,
        MatSelectModule,
        MatMenuModule,
        MatRadioModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTooltipModule,
        ClipboardModule
    ],
    declarations: [
        HomeComponent,
        MenuComponent,
        // PosicionComponent,
        HistoriaComponent,
        BuscarCartaComponent,
        DetalleCartaComponent,
        HistorialAccionesComponent,
        HistorialNoticiasComponent,
        BuscarCartaPatenteComponent,
        CamionesDescargaComponent,
        CondicionesReciboComponent
    ],
    providers: [
        PosicionService
    ]
})
export class HomeModule { }
