import * as moment from 'moment';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Acceso } from '../../models/acceso';
import { LocalStorageService } from '../../services/localStorageService';
import { Router } from '@angular/router';
import { NotificacionesService } from 'src/app/services/notificacionesService';
import { Notificacion } from '../../models/notificacion';

import { constants } from 'src/app/environment/constants';
import { PosicionService } from '../../services/posicionService';
import { PosicionComponent } from '../posicion/posicion.component';
import { CartaPorte } from 'src/app/models/cartaPorte';
import { NoticiasService } from '../../services/noticiasService';
import { Noticia } from 'src/app/models/noticia';
import { UtilsService } from '../../services/utilsService';
import { Puerto } from '../../models/puerto';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    momento = moment;
    options: FormGroup;
    estadoMenu: boolean = true;

    accesoActivo: Acceso = new Acceso();

    menuUserActivo: boolean = false;

    notificaciones: Notificacion[] = [];
    noticias: Noticia[] = [];

    showNotif = false;
    showNoticia = false;

    // Cartas seleccionadas de las notificaciones para hacer alguna accion
    cartasSeleccionadas: CartaPorte[] = [];

    // Anula el blur
    anularBlurNotifi = false;

    constructor(
        fb: FormBuilder,
        public localStorageService: LocalStorageService,
        private router: Router,
        private notificacionesService: NotificacionesService,
        private posicionService: PosicionService,
        private noticiasService: NoticiasService,
        private utilsService: UtilsService
    ) {
        this.options = fb.group({
            'fixed': false,
            'top': 0,
            'bottom': 0,
        });

        this.accesoActivo = localStorageService.getObject('accesoActivo') ? localStorageService.getObject('accesoActivo').acceso : new Acceso();


    }

    ngOnInit() {
        this.estadoMenu = this.localStorageService.getObject('leftMenu');

        // Me subscribo al observable de las notificaciones
        this.notificacionesService.getNotificacionesSubject().subscribe((notifis: Notificacion[]) => {


            // const auxNotisMock = [1,2,3,4,5,6,7];
            // Mock
            // this.notificaciones = auxNotisMock
            //     .map((n, ind) => new Notificacion({
            //         tipo: constants.tiposNotificaciones.DEMORADO,
            //         carta: {
            //             PorteNro: '7777777777',
            //             puertoDestino: {
            //                 ptoRazon: 'FafaPuerto'
            //             }
            //         },
            //         fechaHora: this.momento().add(-ind, 'minutes')
            //     }))

            this.notificaciones = notifis
                .sort((a, b) =>
                    this.momento(a.fechaHora).isAfter(this.momento(b.fechaHora)) ?
                        -1 : 1
                );
        })

        // Me subscribo al observable de las noticias
        this.noticiasService.getNoticiasSubject()
            .subscribe(newNotis => {
                this.noticias = newNotis;
            })

        // Iniciar animacion noticias
        this.noticiasService.initAnimation();

        // Actualizo noticias
        this.noticiasService.actualizar();
    }


    onClickSalir = () => {
        this.localStorageService.clearLocalAndSessionStorage();
        this.router.navigate(['/login']);
    };


    onClickLeftMenu = () => {
        this.estadoMenu = !this.estadoMenu;
        this.localStorageService.setObject('leftMenu')(this.estadoMenu);
    }



    //////////////////////////////////////
    /////////// Notificaciones ///////////
    //////////////////////////////////////
    onClickNotif = () => {

        // if (this.showNotif) {
        //     this.notificaciones = [];
        //     this.notificacionesService.cleanNotificaciones();
        // }

        // Seteo en vistas a todas las notif
        this.notificacionesService.getNotificacionesSubject().next(
            this.notificaciones.map(noti => {
                const cloneNoti = Object.assign({}, noti);
                cloneNoti.vista = true;
                return cloneNoti;
            })
        )
        // this.notificaciones = this.notificaciones.map(noti => {
        //     const cloneNoti = Object.assign({}, noti);
        //     cloneNoti.vista = true;
        //     return cloneNoti;
        // });

        this.showNotif = !this.showNotif;
    }

    onBlurNotif = () => {
        // Si el blur esta anulado, no hago nada y lo des-anulo
        if (this.anularBlurNotifi) {
            this.anularBlurNotifi = false;
        } else {
            // Seteo en vistas a todas las notif
            this.notificaciones = this.notificaciones.map(noti => {
                const cloneNoti = Object.assign({}, noti);
                cloneNoti.vista = true;
                return cloneNoti;
            });

            // En el blur SIEMPRE oculta las notifs
            this.showNotif = false;
        }


    }

    onClickAccionNotif = () => {
        // Mando las acciones
        this.notificacionesService.addCartasParaAccionar(this.cartasSeleccionadas);
        // Limpio las seleccionadas
        this.cartasSeleccionadas = [];
        // Cierro el menu de notificaciones
        this.showNotif = false;
    }

    onClickDetalleCartaNotif = (noti) => {
        this.router.navigate(['/home/detalle-carta', noti.carta.PorteNro])
    }

    onClickCheckboxNoti = (carta) => {
        // Activo control de
        this.anularBlurNotifi = true;

        this.cartasSeleccionadas = this.posicionService.onClickCarta(carta, this.cartasSeleccionadas);
    }

    getNotificacionText = (noti: Notificacion) => `${noti.tipo === constants.tiposNotificaciones.DEMORADO ? 'Demorada' : 'Rechazada'}`
    getCantNotifNoVistas = () => this.notificaciones.filter(noti => !noti.vista).length

    //////////////////////////////////////
    ////////////// Noticias///////////////
    //////////////////////////////////////
    onClickShowNoticia = () => {
        this.showNoticia = !this.showNoticia;
        // Seteo en vistas a todas las notis
        this.noticias = this.noticias.map(noti => {
            const cloneNoti = Object.assign({}, noti);
            cloneNoti.vista = true;
            return cloneNoti;
        });

        if (this.noticias.length > 0) {
            // Seteo estas con vista true en el local storage
            this.noticiasService.setNoticiasStorage(this.noticias);
        }
    }

    onBlurNoticia = () => {
        this.showNoticia = false;
    }

    getCantNotisNoVistas = () => this.noticias.filter(noti => !noti.vista).length

    /**
     * Hago el routing ddesce acá porque tengo que user un (mouseDown) para que cache el click del mouse (ver html)
     */
    onClickNoticia = () => {
        this.router.navigate(['/home/historial-noticias']);
    }
}
