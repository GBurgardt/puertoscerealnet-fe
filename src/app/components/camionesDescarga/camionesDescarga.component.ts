import * as moment from 'moment';
import { Component, OnInit } from '@angular/core';
import { CartaPorteDetalle } from '../../models/cartaPorteDetalle';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/authService';
import { Accion } from 'src/app/models/accion';
import { Observable } from 'rxjs';
import { UtilsService } from 'src/app/services/utilsService';
import { CamionDescarga } from '../../models/camionDescarga';


@Component({
    selector: 'camiones-descarga',
    templateUrl: './camionesDescarga.component.html',
    styleUrls: ['./camionesDescarga.component.scss']
})
export class CamionesDescargaComponent {
    momento = moment;
    camionesDescarga: Observable<CamionDescarga[]>;

    constructor(
        private router: Router,
        private authService: AuthService,
        private utilsService: UtilsService
    ) {
        // Configuro moment
        this.momento.locale('es');

        this.camionesDescarga = authService.getCamionesDescarga();
        // authService.getCamionesDescarga().subscribe(a=>console.log(a));
    }
}
