import * as moment from 'moment';
import { Component, OnInit, Inject } from '@angular/core';
import { CartaPorteDetalle } from '../../models/cartaPorteDetalle';
import { AuthService } from 'src/app/services/authService';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilsService } from 'src/app/services/utilsService';
import { LocalStorageService } from 'src/app/services/localStorageService';

import { Location } from '@angular/common';

import { constants } from '../../environment/constants';
import { CartaPorteService } from 'src/app/services/cartaPorteService';

import * as jsPDF from 'jspdf'
import html2canvas from 'html2canvas';


@Component({
    selector: 'detalle-carta',
    templateUrl: './detalleCarta.component.html',
    styleUrls: ['./detalleCarta.component.scss'],
    providers: [
        { provide: 'Window',  useValue: window }
    ]
})
export class DetalleCartaComponent {
    carta: CartaPorteDetalle;
    momento = moment;

    constants = constants;

    constructor(
        private authService: AuthService,
        private route: ActivatedRoute,
        private utilsService: UtilsService,
        private localStorageService: LocalStorageService,
        private router: Router,
        private location: Location,
        private cartaPorteService: CartaPorteService,
        @Inject('Window') private window: Window
    ) {
        // Configuro moment
        this.momento.locale('es');

        const userLogueado = this.localStorageService.getObject('accesoActivo') ?
            this.localStorageService.getObject('accesoActivo').usuario :
            null;

        // Busco el id del usuario a editar en la ruta
        this.route.params.subscribe(
            params => this.authService.getHistoriaPorNroOrPatente(params.nroCarta).subscribe(
                (cartasEnc: CartaPorteDetalle[]) => {
                    if (cartasEnc.length === 1) {
                        this.carta = cartasEnc[0];
                    } else if (cartasEnc.length > 1) {
                        const cartaCorrecta = cartasEnc.find(cp => cp.Porte_cuit_entregador === userLogueado.cuit);
                        if (cartaCorrecta) {
                            this.carta = cartaCorrecta;
                        } else {
                            this.carta = cartasEnc[0];
                        }
                    } else {
                        this.utilsService.openDefaultDialog('Aviso')('No existe una carta con ese numero');
                        this.router.navigate([`/home/buscar-carta`])
                    }
                }
            )
        );
    }

    /** Parsea el array de análisis a string */
    getAnalisisAll = (analisis) => analisis
        .map(an => this.cartaPorteService.parseAnalisis(an))
        .join(', ');

    isDownloading = false;

    onDownloadPdf = (quality) => {
        const fa: any = document.querySelector('#detalle-carta-pdf').cloneNode(true);

        fa.classList.add('detalle-pdf');

        document.body.appendChild(fa);

        html2canvas(
            fa,
            {scale: quality}
        ).then(canvas => {
			let pdf = new jsPDF('p', 'mm', 'a4');
			pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 211, 298);
            pdf.save(`detalle_${this.carta.Porte_nro}.pdf`);
            this.isDownloading = !this.isDownloading;
        })
    }

}
