import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from 'src/app/services/configService';

@Component({
    selector: 'menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent {

    constructor(
        private router: Router,
        private configService: ConfigService
    ) { }

    changePage = (pagina) => {
        this.router.navigate([`/home/${pagina}`]);
    }

    getClassMenuItem = (ruta) => this.router.url.split('/')[2] === ruta ? 'a-element a-active' : 'a-element'

    toggleTableColor = () => {
        this.configService.toggleTableColor()
    }

}
