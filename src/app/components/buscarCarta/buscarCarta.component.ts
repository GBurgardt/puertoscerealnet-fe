import { Component, OnInit } from '@angular/core';
import { CartaPorteDetalle } from '../../models/cartaPorteDetalle';
import { Router } from '@angular/router';

@Component({
    selector: 'buscar-carta',
    templateUrl: './buscarCarta.component.html',
    styleUrls: ['./buscarCarta.component.scss']
})
export class BuscarCartaComponent {
    nroCartaBuscada: string;

    constructor(
        private router: Router
    ) { }
}
