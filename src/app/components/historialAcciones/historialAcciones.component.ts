import * as moment from 'moment';
import { Component, Inject } from '@angular/core';

import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/authService';
import { Accion } from 'src/app/models/accion';
import { Observable } from 'rxjs';
import { UtilsService } from 'src/app/services/utilsService';
import { WINDOW } from 'src/app/services/windowService';


@Component({
    selector: 'historial-acciones',
    templateUrl: './historialAcciones.component.html',
    styleUrls: ['./historialAcciones.component.scss']
})
export class HistorialAccionesComponent {
    momento = moment;
    nroCartaBuscada: string;

    historial: Accion[];
    historialCompleto: Accion[];

    filtroFecha: {
        fechaDesde: any,
        fechaHasta: any
    } = { fechaDesde: null, fechaHasta: null };

    constructor(
        private router: Router,
        private authService: AuthService,
        private utilsService: UtilsService,
        @Inject(WINDOW) private window: Window
    ) {
        // Configuro moment
        this.momento.locale('es');

        // fechaDesde: this.momento().add(-6, 'days').toDate(),
        this.filtroFecha = {
            fechaDesde: this.momento().toDate(),
            fechaHasta: this.momento().toDate()
        }

        authService.getHistorialAcciones(
            moment(this.filtroFecha.fechaDesde).format("DD/MM/YYYY")
        )(
            this.momento(this.filtroFecha.fechaHasta).add(1, 'days').format("DD/MM/YYYY")
        ).subscribe(historialResp => {
            this.historial = historialResp.slice(0,30);
            this.historialCompleto = historialResp;

            // Activo el infite scroll
            this.processInfiniteScroll();
        });
    }

    /**
     * Filtra por búsqueda
     */
    onBuscar = (e) => {
        if (e && e.length > 0) {
            this.historial = this.historialCompleto.filter(
                carta => carta.nroCarta.toString().includes(e)
            )
        } else {
            this.historial = this.historialCompleto.slice(0, 30);
        }

    }

    /**
     * Activa spinner y hace consulta de nuevo, con nuevas fechas
     */
    onChangeFecha = (e) => {
        // Lo pongo en null para activar spinner
        this.historial = null;

        // Actualizo historial
        this.authService.getHistorialAcciones(
            moment(this.filtroFecha.fechaDesde).format("DD/MM/YYYY")
        )(
            this.momento(this.filtroFecha.fechaHasta).add(1, 'days').format("DD/MM/YYYY")
        ).subscribe(historialResp => {
            this.historial = historialResp.slice(0,30);
            this.historialCompleto = historialResp;
        });
    }

    processInfiniteScroll = () => {
        this.window.addEventListener('scroll', (event) => {
            const sidenavBody = document.getElementsByClassName('sidenav-body mat-drawer-content mat-sidenav-content');

            if (sidenavBody && sidenavBody[0]){

                const posicionFinal = sidenavBody[0].scrollHeight - sidenavBody[0].clientHeight;
                const scrollTop = sidenavBody[0].scrollTop;

                if (posicionFinal - scrollTop <= 0) {
                    const nuevoIndex = (this.historial.length + 30) <= this.historialCompleto.length ? (this.historial.length + 30) : this.historialCompleto.length;

                    const nuevaTanda = this.historialCompleto.slice(0, nuevoIndex);

                    this.historial = nuevaTanda;
                }
            };
        }, true)
    }
}


