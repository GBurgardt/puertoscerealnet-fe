import * as moment from 'moment';
import { Component, OnInit } from '@angular/core';
import { CartaPorteDetalle } from '../../models/cartaPorteDetalle';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/authService';
import { Accion } from 'src/app/models/accion';
import { Observable } from 'rxjs';
import { UtilsService } from 'src/app/services/utilsService';
import { CondicionRecibo } from '../../models/condicionRecibo';


@Component({
    selector: 'condiciones-recibo',
    templateUrl: './condicionesRecibo.component.html',
    styleUrls: ['./condicionesRecibo.component.scss']
})
export class CondicionesReciboComponent {
    momento = moment;
    condicionesRecibo: Observable<CondicionRecibo[]>;

    constructor(
        private router: Router,
        private authService: AuthService,
        private utilsService: UtilsService
    ) {
        // Configuro moment
        this.momento.locale('es');

        this.condicionesRecibo = authService.getCondicionesRecibo();
    }
}
