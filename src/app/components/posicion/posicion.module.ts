import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PosicionComponent } from '../posicion/posicion.component';
import { MatBadgeModule, MatCheckboxModule, MatTableModule, MatPaginatorModule, MatSortModule, MatButtonModule, MatSidenavModule, MatToolbarModule, MatCardModule, MatFormFieldModule, MatInputModule, MatProgressSpinnerModule, MatListModule, MatSelectModule, MatMenuModule, MatRadioModule, MatDatepickerModule, MatNativeDateModule, MatTooltipModule } from '@angular/material';
import { ClipboardModule } from 'ngx-clipboard';
import { PosicionService } from '../../services/posicionService';
import { PosicionRoutingModule } from './home-routing.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatBadgeModule,
        MatCheckboxModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatButtonModule,
        MatSidenavModule,
        MatToolbarModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatListModule,
        MatSelectModule,
        MatMenuModule,
        MatRadioModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTooltipModule,
        ClipboardModule,
        PosicionRoutingModule
    ],
    declarations: [
        PosicionComponent
    ],
    providers: [
        // PosicionService
    ]
})
export class PosicionModule { }
