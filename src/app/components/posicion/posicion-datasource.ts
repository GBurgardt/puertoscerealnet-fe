import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { MatPaginator, MatSort, Sort } from '@angular/material';
import { tap } from 'rxjs/operators';
import { Observable, of as observableOf, merge, BehaviorSubject, of } from 'rxjs';

import { PosicionService } from '../../services/posicionService';
import { CartaPorte } from 'src/app/models/cartaPorte';
import { constants } from 'src/app/environment/constants';



export class PosicionDataSource extends DataSource<CartaPorte> {
    private posicionItemSubject = new BehaviorSubject<CartaPorte[]>([]);
    private loadingSubject = new BehaviorSubject<any>(false);

    public loading$ = this.loadingSubject.asObservable();


    constructor(
        private posicionService: PosicionService
    ) {
        super();
    }


    setPosicion = (posicionArray) => {
        this.posicionItemSubject.next(posicionArray);
    };

    setSpinnerStatus = (status) => this.loadingSubject.next(status);

    getCartasSubject = () => this.posicionItemSubject;

    /////////////////////////////////////////////////////////////
    /////////////////////////// Otros ///////////////////////////
    /////////////////////////////////////////////////////////////
    connect(collectionViewer: CollectionViewer): Observable<CartaPorte[]> {
        return this.posicionItemSubject.asObservable();
    }

    disconnect() {
        this.posicionItemSubject.complete();
        this.loadingSubject.complete();
    }


    sortData = (sort?: Sort) => (posicionTotal?: CartaPorte[]) => (estadoTablaPosicion: number, filtroSituacion?: string) => {
        if (
            sort && (
                !sort.active || sort.direction === ''
            )
        ) {
            return posicionTotal;
        }

        // Si el filtro es 'Con Problemas', entonces SIEMPRE se ponen al final las 'En tramite'
        if (filtroSituacion && filtroSituacion === 'Con problemas') {

            this.posicionItemSubject.next(
                this.posicionItemSubject.value.sort(
                    (a,b) => (a.enTramite === b.enTramite)? 0 : a.enTramite? 1 : -1
                )
            )

        } else {
            // Solo ordena si está filtrada o en infinte_scroll
            if (
                estadoTablaPosicion === constants.estadosTablaPosi.INFINITE_SCROLL
            ) {
                const posiTotalOrdenada = this.ordenar(posicionTotal)(sort);

                // Seteo la nueva posciion ordenada en la tabla (pusheo las primeras constants.infiniteScrollSize nomas)
                this.posicionItemSubject.next(
                    posiTotalOrdenada && posiTotalOrdenada.length >= constants.infiniteScrollSize ?
                        posiTotalOrdenada.slice(0, constants.infiniteScrollSize) :
                        posiTotalOrdenada
                );

                // Retorno la posicion total con su nuevo orden
                return posiTotalOrdenada;
            } else {

                // Ordeno solo lo filtrado
                const posiFiltOrd = this.ordenar(this.posicionItemSubject.value)(sort);
                this.posicionItemSubject.next(posiFiltOrd);


            }
        }


    }


    ordenar = (posi) => (sort) => {
        return posi.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'PorteNro': return this.compare(a.PorteNro, b.PorteNro, isAsc);
                case 'estadoPosiReal': return this.compare(a.estadoPosiReal, b.estadoPosiReal, isAsc);
                case 'porteTurno': return this.compare(a.porteTurno, b.porteTurno, isAsc);
                case 'titular.denominacion': return this.compare(a.titular.denominacion, b.titular.denominacion, isAsc);
                case 'intermediario.denominacion': return this.compare(a.intermediario.denominacion, b.intermediario.denominacion, isAsc);
                case 'remCom.denominacion': return this.compare(a.remCom.denominacion, b.remCom.denominacion, isAsc);
                case 'corredor.denominacion': return this.compare(a.corredor.denominacion, b.corredor.denominacion, isAsc);
                case 'cereal': return this.compare(a.cereal, b.cereal, isAsc);
                case 'puertoDestino.ptoRazon': return this.compare(a.puertoDestino.ptoRazon, b.puertoDestino.ptoRazon, isAsc);
                case 'destin': return this.compare(a.destin, b.destin, isAsc);
                case 'calidadCldNomenclatura': return this.compare(a.calidadCldNomenclatura, b.calidadCldNomenclatura, isAsc);
                case 'analisis': return this.compare(a.analisis, b.analisis, isAsc);
                case 'portePatenteCamion': return this.compare(a.portePatenteCamion, b.portePatenteCamion, isAsc);
                case 'obsEstado': return this.compare(a.obsEstado, b.obsEstado, isAsc);
                case 'procedencia.denominacion': return this.compare(a.procedencia.denominacion, b.procedencia.denominacion, isAsc);
                case 'porteKgsProcede': return this.compare(a.porteKgsProcede, b.porteKgsProcede, isAsc);
                case 'porteKgsNeto': return this.compare(a.porteKgsNeto, b.porteKgsNeto, isAsc);
                case 'entregador.denominacion': return this.compare(a.entregador.denominacion, b.entregador.denominacion, isAsc);
                case 'entreCp': return this.compare(a.entreCp, b.entreCp, isAsc);
                default: return 0;
            }
        });
    }

    compare = (a, b, isAsc) => (a < b ? -1 : 1) * (isAsc ? 1 : -1);


}

