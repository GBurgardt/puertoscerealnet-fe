import * as moment from 'moment';
import { catchError, finalize } from 'rxjs/operators';
import { of, interval } from 'rxjs';

import { Component, OnInit, Inject } from '@angular/core';

import { PosicionDataSource } from './posicion-datasource';

import { PosicionService } from '../../services/posicionService';

import { FiltrosModel } from 'src/app/models/filtrosModel';
import { ListaDespModel } from 'src/app/models/listaDespModel';

import { constants } from 'src/app/environment/constants';
import { Estado } from '../../models/estado';
import { Puerto } from 'src/app/models/puerto';
import { TipoAccion } from 'src/app/models/tipoAccion';
import { CartaPorte } from 'src/app/models/cartaPorte';
import { Notificacion } from 'src/app/models/notificacion';
import { CartaPorteService } from 'src/app/services/cartaPorteService';
import { columnasTablaPosicion } from 'src/app/components/posicion/posicion.constantes';
import { WINDOW } from 'src/app/services/windowService';
import { LocalStorageService } from '../../services/localStorageService';
import { NotificacionesService } from 'src/app/services/notificacionesService';
import { NoticiasService } from 'src/app/services/noticiasService';
import { ConfigService } from 'src/app/services/configService';

@Component({
    selector: 'posicion',
    templateUrl: './posicion.component.html',
    styleUrls: ['./posicion.component.scss']
})
export class PosicionComponent implements OnInit {
    // Data de la tabla
    dataSource: PosicionDataSource;

    // Columnas
    displayedColumns = columnasTablaPosicion;

    posicionTotal: CartaPorte[] = [];

    // Objeto contenedor de las listas desplegables
    listasDesp: ListaDespModel = new ListaDespModel();

    // Los filtros activos actualmente (Siempre se filtra sobre la poscionTotal)
    filtrosActivos: FiltrosModel = new FiltrosModel();

    // Total de cartas activas (este se actualzia cuando se filtra)
    totalActivo: number = 0;

    // Cartas seleccionadas para hacer alguna accion
    cartasSeleccionadas: CartaPorte[] = [];

    // Estado de la tabla
    estadoTablaPosicion: number = constants.estadosTablaPosi.INFINITE_SCROLL;

    // Valor de busqueda activo
    valueBusqueda: string = '';

    // Hora de la última actualizacion registrada
    ultimaActualizacion: any;

    // Spinenr auxiliar. Necesario porque el spinner del datasource anda complicadamente. TODO: revisar
    recargandoPosi: boolean = false;

    constructor(
        public posicionService: PosicionService,
        public cartaPorteService: CartaPorteService,
        @Inject(WINDOW) private window: Window,
        private localStorageService: LocalStorageService,
        private notificacionesService: NotificacionesService,
        private noticiasService: NoticiasService,
        private configService: ConfigService
    ) {
        // Configuro moment
        moment.locale('es');
    }

    ngOnInit() {
        // Inicializo algunos campos importantes de esta page
        this.initTable();

        // Cartas para accionar que vienen desde las notificaciones (quedo observando si vien alguna)
        this.notificacionesService.getCartasParaAccionarSubject().subscribe(cartasParaAccionar => {

            if (cartasParaAccionar && cartasParaAccionar.length > 0) {
                this.cartasSeleccionadas = cartasParaAccionar;
                this.posicionService.openAccionDialog(this.cartasSeleccionadas, this.actualizarDataDspsDeAccion)
            }
        })

    };


    initTable = () => {
        // Inicializo el dataSource
        this.dataSource = new PosicionDataSource(
            this.posicionService
        );

        // Prendo spinner
        this.dataSource.setSpinnerStatus(true);

        // Hago la primer consulta al service. Aca agarra X cantidad de cartas, para arrancar
        this.posicionService.getPosicion().pipe(
            catchError(() => of([]))
        )
        .subscribe(
            async(posicionDia: CartaPorte[]) => {

                // Me fijo si hay filtros activos en la sesión
                const filtrosSesion = this.localStorageService.getSessionObject('filtrosActivos');

                // Primero, si hay filtros activos en la sesión, los aplico
                let posiFiltrada = posicionDia;
                if (filtrosSesion) {
                    this.filtrosActivos = filtrosSesion;
                    posiFiltrada = this.cartaPorteService.filtrar(posicionDia, this.filtrosActivos);
                    // Si el tamaño es distinto quiere decir que algo filtró, por lo que cambio el estado
                    this.estadoTablaPosicion = posiFiltrada.length < posicionDia.length ?
                        constants.estadosTablaPosi.FILTRADA : constants.estadosTablaPosi.INFINITE_SCROLL
                }

                // Guardo datos importantes
                this.posicionTotal = posicionDia;
                this.listasDesp = this.cartaPorteService.getArraysIniciales(posicionDia);

                // Gaurdo el total de lo filtrado
                this.totalActivo = posiFiltrada.length;

                // Mando una primer tanda de constants.infiniteScrollSize
                const primerTanda = posiFiltrada.slice(0, constants.infiniteScrollSize);

                // Mando primer tanda a la tabla
                this.dataSource.setPosicion(primerTanda);

                // Inicio el infinite scroll
                this.processInfiniteScroll();

                // Actualizo hora de actualizacion
                this.ultimaActualizacion = moment();

                // Saco spinner
                this.dataSource.setSpinnerStatus(false)

                // Ordeno la tabla
                if (this.filtrosActivos && this.filtrosActivos.situacion === 'Con problemas') {
                    this.sortTabla(null);
                }
            }
        );

        // // Actualizo noticias
        // this.cartaPorteService.actualizarNoticias();

        // Inicializo intervalo, cada 3 min recarga posicion
        this.recargaAutomatica();
    }

    processInfiniteScroll = () => {
        this.window.addEventListener('scroll', (event) => {
            this.posicionService.processInfiniteScroll(this.posicionTotal)(this.dataSource)(this.estadoTablaPosicion);
        }, true)
    }

    // Actualiza posicion cada X segundos
    recargaAutomatica = () => {
        const source = interval(240000); // 3 minutos

        source.subscribe(val => {
            console.log('Recarga automatica')
            this.reloadData();
        });
    }


    /**
     * Filtrar las cartas
     */
    filtrar = (filtro, tipoFiltro) => {
        // Seteo los filtros activos
        this.filtrosActivos = this.cartaPorteService.getFiltrosActivos(this.filtrosActivos)(tipoFiltro)(filtro);

        // Guardo los filtros activos en la sessión actual
        this.localStorageService.setSessionObject('filtrosActivos')(this.filtrosActivos);

        // Filtro
        const posiFiltrada = this.cartaPorteService.filtrar(this.posicionTotal, this.filtrosActivos);

        // Actualizo el estado de la tabla. Tambien checkeo si se sacaron todos los filtros (la tabla vuelve a tener la posicion total)
        if (posiFiltrada.length < this.posicionTotal.length) {
            // Seteo nueva posicion filtrada
            this.dataSource.setPosicion(posiFiltrada);
            // Actualizo estado
            this.estadoTablaPosicion = constants.estadosTablaPosi.FILTRADA;
        } else {
            // Seteo posicion total (agarro solo constants.infiniteScrollSize primeras cartas)
            this.dataSource.setPosicion(posiFiltrada.slice(0, constants.infiniteScrollSize));
            // Actualizo estado
            this.estadoTablaPosicion = constants.estadosTablaPosi.INFINITE_SCROLL;
        }
        // Actualizo el total activo
        this.totalActivo = posiFiltrada.length;

        // Ordeno de nuevo
        this.sortTabla(null)
    }


    /**
     * On buscar
     */
    onBuscar = (newValue) => {

        // Filtro por búsqueda (si ya está filtrada, busco en filtrada. Sinó, busco en total)
        const posiFiltPorBusq = this.cartaPorteService.filtroBusqueda(
            this.estadoTablaPosicion === constants.estadosTablaPosi.INFINITE_SCROLL ?
                this.posicionTotal :
                this.cartaPorteService.filtrar(this.posicionTotal, this.filtrosActivos)
            , newValue
        );

        // Seteo nueva posicion filtrada
        this.dataSource.setPosicion(posiFiltPorBusq);

        // Actualizo estado
        this.estadoTablaPosicion = constants.estadosTablaPosi.FILTRADA;

        // Actualizo el total activo
        this.totalActivo = posiFiltPorBusq.length;

    }


    /**
     * Selecciona o deselecciona cartas
     */
    onClickCarta = (row: CartaPorte) => {
        this.cartasSeleccionadas = this.posicionService.onClickCarta(row, this.cartasSeleccionadas);
    }

    /**
     * Genera el excel
     */
    generarExcel = () => {
        // Guardo el estado actual
        const estadoAnterior = this.estadoTablaPosicion;
        // Ahora pongo el estado en Descargando
        this.estadoTablaPosicion = constants.estadosTablaPosi.DESCARGANDO;
        // Genero el excel
        this.cartaPorteService.generarExcel(this.dataSource)(estadoAnterior)('posicion')()().subscribe(
            nuevoEstado => this.estadoTablaPosicion = nuevoEstado
        );
    }

    /**
     * Actualial a data despues de ejercer una accion
     * cartasDeNotificacion: Cartas selecionadas en notificacion
     */
    actualizarDataDspsDeAccion = (resultado: {
        puertoNuevo: Puerto,
        comentario: string,
        tipoAccion: TipoAccion
    }, cartasDeNotificacion) => {

        // Si Confirmó, resultado viene con algo. Sinó, resultado viene undefined
        if (resultado) {

            const dataActualizada = this.posicionTotal.map(carta => {
                const newCarta = this.cartasSeleccionadas.find(
                    cartaSelec =>   carta.PorteNro === cartaSelec.PorteNro &&
                                    carta.portePrefijo === cartaSelec.portePrefijo &&
                                    carta.entregador.cuit === cartaSelec.entregador.cuit &&
                                    carta.porteVagon === cartaSelec.porteVagon &&
                                    carta.porteCodPuerto === cartaSelec.porteCodPuerto
                );
                if (newCarta) {

                    const nuevoEstado =     resultado.tipoAccion.tipo === 1 ? 10 :
                                            resultado.tipoAccion.tipo === 2 ? 11 :
                                            resultado.tipoAccion.tipo === 4 ? 8 :
                                            0;

                    const nuevoEstadoDescrip =      resultado.tipoAccion.tipo === 1 ? 'Entrega Autorizado' :
                                                    resultado.tipoAccion.tipo === 2 ? 'Entrega Desviado' :
                                                    resultado.tipoAccion.tipo === 4 ? 'Rec. Oficial' :
                                                    'Posicion';

                    newCarta.estado = new Estado({estado: nuevoEstado, descripcion: nuevoEstadoDescrip});
                    newCarta.estadoPosiReal = nuevoEstadoDescrip;
                    return newCarta;
                } else {
                    return carta;
                }
            });

            // Primero, si hay filtros activos, filtro la posicion antes de setearla en la tabla
            // Si no hay filtros entonces pusheo la primer tanda del nuevo array
            this.dataSource.setPosicion(
                this.estadoTablaPosicion == constants.estadosTablaPosi.FILTRADA ?
                    this.cartaPorteService.filtrar(dataActualizada, this.filtrosActivos) :
                    dataActualizada.length <= constants.infiniteScrollSize ?
                        dataActualizada : dataActualizada.slice(constants.infiniteScrollSize)
            );

            // Limpio las cartas previamente seleccionadas (Solo limpio en caso de que confirme. Si cancela, entonces quedan seleccionadas las anteriores)
            this.cartasSeleccionadas = [];

            // Remuevo las accionadas de las notificaciones
            this.notificacionesService.removeFromNotificaciones(cartasDeNotificacion);

            // Remuevo notificaciones obsoletas
            this.notificacionesService.removerNotifObsoletas(
                this.cartaPorteService.getConProblemas(dataActualizada)
            )

        }
    }


    /**
     * Ordena la tabla y actualiza la posi total
     */
    sortTabla = (e) => {
        if (this.estadoTablaPosicion === constants.estadosTablaPosi.INFINITE_SCROLL) {
            this.posicionTotal = this.dataSource.sortData(e)(this.posicionTotal)(this.estadoTablaPosicion)
        } else {
            // Si la posi está filtrada NO necesito actualizar la posicionTotal que tengo acá guardada
            this.dataSource.sortData(e)()(this.estadoTablaPosicion, this.filtrosActivos.situacion)
        }
    }

    /**
     * Recarga la data de la tabla
     */
    reloadData = (showSpinner: boolean = false) => {
        // Actualizo hora de actualizacion
        this.ultimaActualizacion = moment();

        // Limpio los seleccionados para accionar
        this.cartasSeleccionadas = [];

        const auxPosicionObservable = this.posicionService.getPosicion();

        // Muestro spinner (o no)
        this.recargandoPosi = showSpinner;

        // Actualizo noticias push
        this.noticiasService.actualizar();
        // this.cartaPorteService.actualizarNoticias();

        // Actualizo posicion y otros
        auxPosicionObservable
            .subscribe(
                async(posiActualizada) => {
                    // Actualizo notificaciones
                    this.cartaPorteService.actualizarNotificaciones(this.posicionTotal)(posiActualizada);

                    // Primero, si hay filtros activos, filtro la posicion antes de setearla en la tabla
                    const posiFiltrada = this.estadoTablaPosicion == constants.estadosTablaPosi.FILTRADA ?
                        this.cartaPorteService.filtrar(posiActualizada, this.filtrosActivos) :
                        posiActualizada;

                    // Seteo la nueva posicion en el observable de la tabla
                    this.dataSource.setPosicion(
                        this.estadoTablaPosicion == constants.estadosTablaPosi.FILTRADA ? posiFiltrada : posiActualizada.slice(0, constants.infiniteScrollSize)
                    );

                    // Guardo la nueva posi en el array que luego uso para filtrar
                    this.posicionTotal = posiActualizada;
                    // Obtengo de nuevo las listas desplegables
                    this.listasDesp = await this.cartaPorteService.getArraysIniciales(posiActualizada);
                    // Obtengo de nuevo la cantidad a mostrar
                    this.totalActivo = posiFiltrada.length;

                    this.recargandoPosi = false;

                    // Ordeno de nuevo
                    this.sortTabla(null);
                }
            )
    }

    getCorredorRow = (row) =>
        `${
            row.corredor && row.corredor.denominacion && row.corredor.denominacion.length > 0 ?
                this.cartaPorteService.acotarString(row.corredor.denominacion)() : ''
        }
        ${
            row.corredor2 && row.corredor2.denominacion && row.corredor2.denominacion.length > 0 ?
                ' | ' : ''}
        ${
            row.corredor2 && row.corredor2.denominacion && row.corredor2.denominacion.length > 0 ?
                this.cartaPorteService.acotarString(row.corredor2.denominacion)() : ''
        }`.toLowerCase();


    /**
     * Fafa fa fafafa fa fa fafafafafa fafa
     */
    onChangeEnTramite = (row: CartaPorte, newState: boolean) => {
        this.posicionService.setEnTramite(row, newState).subscribe(
            resp => this.sortTabla(null)
        )
    }

    /**
     * Retorna un texto custom cuando click en whasap btn
     */
    getCopyRowDescrip = (row: CartaPorte) => {
        const analisisRow = document.getElementById(`idAnalisis-${row.PorteNro}`).innerText;
        return `Producto: ${row.cereal}, Estado: ${row.estadoPosiReal ? row.estadoPosiReal : ''}, Puerto: ${row.puertoDestino && row.puertoDestino.ptoRazon ? row.puertoDestino.ptoRazon.trim() : ''}, Procedencia: ${row.procedencia && row.procedencia.denominacion ? row.procedencia.denominacion.trim() : ''}, Titular: ${row.titular && row.titular.denominacion ? row.titular.denominacion.trim() : ''}, Nro Carta: ${row.PorteNro}, ${row.obsEstado ? row.obsEstado : ''} ${row.porteObservacion ? row.porteObservacion.trim() : ''} ${analisisRow ? analisisRow : ''}, Calidad: ${row.calidadCldNomenclatura ? row.calidadCldNomenclatura : ''}`;
    }



    ////////////////////////////////////////////////////////////////////////////////////
    //////////////////// CompareWiths, setea filtro que viene de sesión ////////////////
    ////////////////////////////////////////////////////////////////////////////////////
    //       Tienen que ser funciones separadas porque angular es una gadorcha        //
    setDefaultSelectEspe = (ob1, ob2) => this.filtrosActivos.especie === ob1.codmerca;
    setDefaultSelectDest = (ob1, ob2) => this.filtrosActivos.destino === ob1.ptoCodinterno;
    setDefaultSelectSitu = (ob1, ob2) => this.filtrosActivos.situacion === ob1;
    setDefaultSelectEntre = (ob1, ob2) => this.filtrosActivos.entregador === ob1.cuit;
    setDefaultSelectTitu = (ob1, ob2) => this.filtrosActivos.titular === ob1.cuit;
    setDefaultSelectInter = (ob1, ob2) => this.filtrosActivos.intermediario === ob1.cuit;
    setDefaultSelectRemCom = (ob1, ob2) => this.filtrosActivos.remComercial === ob1.cuit;
    setDefaultSelectCorre = (ob1, ob2) => this.filtrosActivos.corredor === ob1.cuit;
    setDefaultSelectDesti = (ob1, ob2) => this.filtrosActivos.destinatario === ob1.cuit;
    setDefaultSelectProce = (ob1, ob2) => this.filtrosActivos.procedencia === ob1.cod;


}

