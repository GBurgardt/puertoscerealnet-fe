import * as moment from 'moment';
import { Component, Inject } from '@angular/core';

import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/authService';
import { UtilsService } from 'src/app/services/utilsService';
import { WINDOW } from 'src/app/services/windowService';
import { Noticia } from '../../models/noticia';


@Component({
    selector: 'historial-noticias',
    templateUrl: './historialNoticias.component.html',
    styleUrls: ['./historialNoticias.component.scss']
})
export class HistorialNoticiasComponent {
    momento = moment;
    nroCartaBuscada: string;

    historial: Noticia[];
    historialCompleto: Noticia[];

    filtroFecha: {
        fechaDesde: any,
        fechaHasta: any
    } = { fechaDesde: null, fechaHasta: null };

    constructor(
        private router: Router,
        private authService: AuthService,
        private utilsService: UtilsService,
        @Inject(WINDOW) private window: Window
    ) {
        // Configuro moment
        this.momento.locale('es');

        // fechaDesde: this.momento().add(-6, 'days').toDate(),
        this.filtroFecha = {
            fechaDesde: this.momento().toDate(),
            fechaHasta: this.momento().toDate()
        }

        // Obtengo fecha formateada (últimas noticas de los últimos 3 días)
        const haceTresDias = moment()
            .subtract(3, 'days')
            .format('YYYYMMDDHHmmss');

        authService.getHistorialNoticias(haceTresDias)
            .subscribe(historialResp => {
                this.historial = historialResp.slice(0,30);
                this.historialCompleto = historialResp;

                // Activo el infite scroll
                this.processInfiniteScroll();
            });
    }

    processInfiniteScroll = () => {
        this.window.addEventListener('scroll', (event) => {
            const sidenavBody = document.getElementsByClassName('sidenav-body mat-drawer-content mat-sidenav-content');

            if (sidenavBody && sidenavBody[0]){

                const posicionFinal = sidenavBody[0].scrollHeight - sidenavBody[0].clientHeight;
                const scrollTop = sidenavBody[0].scrollTop;

                if (posicionFinal - scrollTop <= 0) {
                    const nuevoIndex = (this.historial.length + 30) <= this.historialCompleto.length ? (this.historial.length + 30) : this.historialCompleto.length;

                    const nuevaTanda = this.historialCompleto.slice(0, nuevoIndex);

                    this.historial = nuevaTanda;
                }
            };
        }, true)
    }
}


